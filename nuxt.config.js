export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'landing-page',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['~/assets/styles/vendor.scss', '~/assets/styles/main.scss'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},

  pwa: {
    meta: {
      favicon: 'shortcut icon' + 'apple-touch-icon',
      mobileApp: 'mobile-web-app-capable',
      mobileAppIOS: 'apple-mobile-web-app-capable',
      appleStatusBarStyle: 'black_transparent',
      nativeUI: 'true',
    },
    manifest: {
      name: 'El secreto | Comida',
      short_name: 'landing',
      lang: 'es',
      theme_color: '#ffffff',
      background_color: '#D8AD3D',
      display: 'standalone',
      icons: [
        {
          src: '/icon-48x48.png',
          sizes: '48x48',
          type: 'image/png',
        },
        {
          src: '/icon-72x72.png',
          sizes: '72x72',
          type: 'image/png',
        },
        {
          src: '../icon-96x96.png',
          sizes: '96x96',
          type: 'image/png',
        },
        {
          src: '../icon-128x128.png',
          sizes: '128x128',
          type: 'image/png',
        },
        {
          src: '../icon-144x144.png',
          sizes: '144x144',
          type: 'image/png',
        },
        {
          src: '../icon-152x152.png',
          sizes: '152x152',
          type: 'image/png',
        },
        {
          src: '../icon-192x192.png',
          sizes: '192x192',
          type: 'image/png',
        },
        {
          src: '../icon-384x384.png',
          sizes: '384x384',
          type: 'image/png',
        },
        {
          src: '../icon-512x512.png',
          sizes: '512x512',
          type: 'image/png',
        },
      ],
    },
  },
}
